#ifndef TREE_H
#define TREE_H
#include "Vector3.h"

class Tree
{
public:
	float xpos;
	float ypos;
	float zpos;
	float scaleamt;

	float minx;
	float maxx;
	float miny;
	float maxy;
	float minz;
	float maxz;
	Tree();
	Tree(Vector3 pos,float scale);
	~Tree();
};


#endif
