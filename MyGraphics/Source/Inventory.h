#ifndef INVENTORY_H
#define INVENTORY_H
#include <windows.h>
#include <iostream>
#include <string>
#include "Scene.h"
class Items;

class Inventory
{
public:
	~Inventory();
	
	float rotate;
	static Inventory* getInstance();
	void move(int x);
	void additem(int x);
	void print();
	void func();
	void update(double dt);
	Items* current;
private:
	Inventory();
	static Inventory* invinstance;
	Items* tail;
	Items* head;
	
};


class Items
{
public:
	Items();
	std::string Name;
	int amount;
	enum state
	{
		front,
		back,
		idle,
	};
	int state;
	Items* next;
	Items* prev;
	virtual void func() = 0;
	virtual void melee();
	virtual void shoot();
	~Items();
private:

};

class Spear :public Items
{
public:
	Spear();
	virtual void func();
	~Spear();
private:
};

class Sword :public Items
{
public:
	Sword();
	virtual void func();
	~Sword();
private:
};

class Bow :public Items
{
public:
	Bow();
	virtual void func();
	~Bow();
private:
};

class Arrow :public Items
{
public:
	static Arrow& getInstance();
	virtual void func();	
private:
	Arrow();
	~Arrow();
};
#endif
