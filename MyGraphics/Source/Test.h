#ifndef TEST_H
#define TEST_H
#include <iostream>


class AITYPES
{
public:
	AITYPES() {}
	virtual void funct() = 0;
	int range;
	~AITYPES(){}
};

class DINO : public AITYPES
{
public:
	DINO() {
	}
	virtual void funct();
};
class CAVEMAN : public AITYPES
{
public:
	CAVEMAN() {
	}
	virtual void funct();
};

class funcx
{
public:
	enum AITYPESs
	{
		RELIC,
	};
	funcx()
	{
		AITYPES_ = nullptr;
	}
	void choosefunc(int type);
	void dofunc()
	{
		AITYPES_->funct();
	}
private:
	AITYPES* AITYPES_;
};
#endif