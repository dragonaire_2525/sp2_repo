#include "Tree.h"

Tree::Tree()
{
	xpos = 1;
	ypos = 1;
	zpos = 1;
	scaleamt = 1;
	minx=1;
	maxx=1;
	miny=1;
	maxy=1;
	minz=1;
	maxz=1;
}

Tree::Tree(Vector3 pos, float scale)
{
	xpos = pos.x;
	ypos = pos.y;
	zpos = pos.z;
	scaleamt = scale;
	minx = xpos - 1 * scale;
	maxx = xpos + 1 * scale;
	miny = ypos - 1 * scale;
	maxy = ypos + 1 * scale;
	minz = zpos - 1 * scale;
	maxz = zpos + 1 * scale;
}

Tree::~Tree()
{

}