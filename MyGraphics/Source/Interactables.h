#ifndef INTERACT_H
#define INTERACT_H
#include "Vector3.h"

struct interact
{
	Vector3 position;
	Vector3 size;

	float minX;
	float maxX;

	float minY;
	float maxY;

	float minZ;
	float maxZ;

	interact(Vector3 _position, Vector3 _size)
	{
		size = _size;
		position = _position;
		minX = position.x - _size.x;
		maxX = position.x + _size.x;
		minY = position.y - _size.y;
		maxY = position.y + _size.y;
		minZ = position.z - _size.z;
		maxZ = position.z + _size.z;
	}
	bool inrange(Vector3 _position)
	{
		Vector3 pos = _position;
		if (((pos.x <= maxX) && (pos.x >= minX)) && ((pos.y <= maxY) && (pos.y >= minY)) && ((pos.z <= maxZ) && (pos.z >= minZ)))
		{
			return true;
		}
		else return false;
	}
};
#endif
