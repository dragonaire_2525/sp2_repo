
#ifndef APPLICATION_H
#define APPLICATION_H

#include "timer.h"

class Application
{
public:
	Application();
	~Application();
	void Init();
	void Run();
	void Exit();
	static int MouseScroll();
	static void ResetScroll();
	static bool IsKeyPressed(unsigned short key);
	static bool IsPressedOnce(unsigned short key);
	static bool IsMousePressed();
	static void GetMousePos(double & xpos, double & ypos);
	static void SetMousePos();

private:

	//Declare a window object
	StopWatch m_timer;
};

#endif