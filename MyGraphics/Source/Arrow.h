#ifndef ARROW_H
#define ARROW_H
#include "Vector3.h"
class arrow
{
public:
	float magnitude;
	Vector3 vector2d;
	Vector3 position;
	Vector3 parabola;
	Vector3 direction;
	float yrotate;
	float xrotate;
	double displacement;
	arrow() { displacement = 0;
	magnitude = 0;
	}
	~arrow(){}
};
#endif
