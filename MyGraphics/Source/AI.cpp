#include "AI.h"

AI::AI(Vector3 position_, float range, Vector3 _size)
{
	spd = 0.2f;
	size = _size;
	this->range = range;
	o_range = 50.f;
	position = position_;
	origin = position;
	minX = position.x - _size.x;
	maxX = position.x + _size.x;
	minY = position.y - _size.y;
	maxY = position.y + _size.y;
	minZ = position.z - _size.z;
	maxZ = position.z + _size.z;
	check = false;
	inrangeb = false;
}
void AI::inrange(Vector3 _position)
{
	Vector3 pos = _position;
	if (((pos.x <= position.x + range) && (pos.x >= position.x - range)) && ((pos.y <= position.y + range) && (pos.y >= position.y - range)) && ((pos.z <= position.z + range) && (pos.z >= position.z - range)))
	{
		inrangeb = true;
	}
	else inrangeb = false;
}
void AI::move(Vector3 camera)
{

	Vector3 dir;
	Vector3 tmppos = position;

	maginitude = Waypoints[Way] - position;
	float magnitudefloat = maginitude.Length();

	//if (WayCount == 0)
	//{
	//	this->position = this->position + Directions[0].Normalized() * 0.1;
	//	
	//}
	//if (WayCount == 1)
	//{
	//	this->position = this->position + Directions[1].Normalized() * 0.1;
	//}


	if (inrangeb == true)
	{
		dir = camera - position;
		position += dir.Normalized() * spd;
	}
	else if (inrangeb == false) {
		if (magnitudefloat <= 1) {

			if (WayCount == 0) {
				WayCount = 1;
				Way = 2;

			}
			else if (WayCount == 1) {
				WayCount = 2;
				Way = 3;
			}
			else if (WayCount == 2) {
				WayCount = 3;
				Way = 0;
			}
			else if (WayCount == 3) {
				WayCount = 0;
				Way = 1;
			}
		}


		if (WayCount == 0) {
			this->position = this->position + Directions[0].Normalized() * 0.1;
		}

		if (WayCount == 1) {
			this->position = this->position + Directions[1].Normalized() * 0.1;
		}
		if (WayCount == 2) {
			this->position = this->position + Directions[2].Normalized() * 0.1;
		}
		if (WayCount == 3) {
			this->position = this->position + Directions[3].Normalized() * 0.1;
		}





		std::cout << magnitudefloat << std::endl;
	}

	//std::cout << magnitudefloat << std::endl;
	/*if (inrangeb == true)
	{
	dir = camera - position;
	position += dir.Normalized() * spd;
	}
	else
	{
	if (check == true)
	{
	dir = origin - position;
	position += dir.Normalized() * spd;
	if ((position.x < origin.x) || (position.z < origin.z) || (position.z > origin.z))
	{
	check = false;
	}
	}

	else
	{
	dir = Vector3(1, 0, 0);
	position += dir.Normalized() * spd;

	if ((position.x > origin.x + o_range) || (position.x < origin.x - o_range) || (position.y < origin.y - o_range) || (position.y > origin.y + o_range) || (position.z < origin.z - o_range) || (position.z > origin.z + o_range))
	{
	check = true;
	}
	}

	}*/

	dir = position - tmppos;
	dir = dir.Normalized() * spd;
	minX = position.x - size.x;
	maxX = position.x + size.x;
	minY = position.y - size.y;
	maxY = position.y + size.y;
	minZ = position.z - size.z;
	maxZ = position.z + size.z;


}
void AI::SetWayPoint(Vector3 pos) {
	// Adding ONE waypoint's position into the array
	Waypoints.push_back(pos);
}
void AI::SetDirection(Vector3 dir) {
	// adds one direction into the array
	Directions.push_back(dir);
}
AI::~AI()
{

}