#include "Inventory.h"

//Inventory
Inventory::Inventory()
{
	rotate = 0;
	head = NULL;
	current = NULL;
	tail = NULL;
}
Inventory* Inventory::getInstance()
{
	if (invinstance != NULL)
		return invinstance;
	else
	{
		invinstance = new Inventory;
		return invinstance;
	}
}

Inventory::~Inventory()
{
	if (head != NULL)
	{
		while (head->next != NULL)
		{
			current = head->next;
			delete head;
			head = current;
			if (head = tail)
			{
				delete head;
				break;
			}
		}
	}
}

void Inventory::additem(int item_)
{
	Items* temp;
	switch (item_)
	{
	case Scene::GEO_SWORD:
		if (head == NULL)
		{
			head = new Spear;
			head->next = head;
			head->prev = head;
			current = head;
			tail = head;
		}
		else
		{
			temp = new Spear;
			temp->next = head->next;
			temp->prev = head;
			head->next->prev = temp;
			head->next = temp;
			tail = head->prev;
		}
		break;
	case Scene::GEO_SPEAR:
		if (head == NULL)
		{
			head = new Spear;
			head->next = head;
			head->prev = head;
			current = head;
			tail = head;
		}
		else
		{
			temp = new Spear;
			temp->next = head->next;
			temp->prev = head;
			head->next->prev = temp;
			head->next = temp;
			tail = head->prev;
		}
		break;
	case Scene::GEO_BOW:
		if (head == NULL)
		{
			head = new Bow;
			head->next = head;
			head->prev = head;
			current = head;
			tail = head;
		}
		else
		{
			temp = new Bow;
			temp->next = head->next;
			temp->prev = head;
			head->next->prev = temp;
			head->next = temp;
			tail = head->prev;
		}
		break;
	case Scene::GEO_ARROW:
		if (head == NULL)
		{
			head = &Arrow::getInstance();
			head->amount += 5;
			head->next = head;
			head->prev = head;
			current = head;
			tail = head;
		}
		else
		{
			temp = &Arrow::getInstance();
			if (temp->amount == 0)
			{
				temp->amount = 5;
				temp->next = head->next;
				temp->prev = head;
				head->next->prev = temp;
				head->next = temp;
				tail = head->prev;
			}
			else
				temp->amount += 5;
		}
		break;
	default:
		break;
	}
}

void Inventory::func()
{
	current->func();
}

void Inventory::print()
{
	if (current != NULL)
	{
		std::cout << current->Name << "  " << current->amount << std::endl;
	}
}
void Inventory::move(int x)
{
	if (head != NULL)
	{
		if (x > 0)
		{
			for (int i = 0; i < x;i++)
			{
				current = current->next;
			}
		}
		else if (x < 0)
		{
			for (int i = 0; i > x;i--)
			{
				current = current->prev;
			}
		}
	}
}

void Inventory::update(double dt)
{
	if (current->state == Items::front)
	{
		if (rotate < 0.08)
			rotate += dt;
		else
			current->state = Items::back;
	}
	if (current->state == Items::back)
	{
		if (rotate > 0.009)
			rotate -= dt;
		else
			current->state = Items::idle;
	}
	Items* temp;
	Items* tmp;
	temp = head;
	if (head != NULL)
	{
		while (temp->next!= tail)
		{
			if (temp->next->amount == 0)
			{
				tmp = temp->next->next;
				if (current == temp->next)
					current = current->next;
				delete temp->next;
				temp->next = tmp;
			}
			else
				temp = temp->next;
		}
		if (head != tail)
		{
			if (head->amount == 0)
			{
				temp = head;
				head = head->next;
				if (current == temp)
					current = current->next;
				delete temp;
			}
		}
		else
		{
			if (head->amount == 0)
				delete head;
			head = NULL;
			tail = NULL;
			current = NULL;
		}
	}
}











//----------Items-----------
Items::Items()
{
	next = NULL;
	prev = NULL;
}
void Items::melee()
{
	if (state == idle)
	{
		state = front;
	}

	
}
void Items::shoot()
{
}
Items::~Items()
{
}

//Spear
Spear::Spear()
{
	Name = "Spear";
	amount = 1;
	state = idle;
	next = NULL;
	prev = NULL;
}
void Spear::func()
{
	melee();
}
Spear::~Spear()
{
}

//Sword
Sword::Sword()
{
	Name = "Sword";
	amount = 1;
	state = idle;
	next = NULL;
	prev = NULL;
}
void Sword::func()
{
	melee();
}
Sword::~Sword()
{
}

//Bow
Bow::Bow()
{
	Name = "Bow";
	amount = 1;
	state = idle;
	next = NULL;
	prev = NULL;
}
void Bow::func()
{
	shoot();
	std::cout << "ahahahahahaa" << std::endl;
}
Bow::~Bow()
{
}

//Arrow
Arrow::Arrow()
{
	Name = "Arrows";
	amount = 0;
	state = idle;
	next = NULL;
	prev = NULL;
}
void Arrow::func()
{
	amount += 5;
}
Arrow& Arrow::getInstance()
{
	static Arrow Instance;
	return Instance;
}
Arrow::~Arrow()
{
}