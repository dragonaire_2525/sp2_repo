#include "Inventory.h"

//Inventory
Inventory::Inventory()
{
	for (int i = 0;i < NUM_GEOMETRY;i++)
	{
		item[i] = NULL;
	}
}

Inventory::~Inventory()
{
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (item[i] != NULL)
			delete item[i];
		item[i] = NULL;
	}
}

void Inventory::additem(int item_)
{
	switch (item_)
	{
	case GEO_SWORD:
		if (item[GEO_SWORD] == NULL)
			item[GEO_SWORD] = (new Spear);
		break;
	case GEO_SPEAR:
		if(item[GEO_SPEAR] == NULL)
		item[GEO_SPEAR] = (new Spear);
		break;
	case GEO_BOW:
		if (item[GEO_BOW] == NULL)
		item[GEO_BOW] = (new Bow);
		break;
	case GEO_ARROW:
		if (item[GEO_ARROW] == NULL)
			item[GEO_ARROW] = (new Arrow);
		else
			item[GEO_ARROW]->func();
		break;
	default:
		break;
	}
}

void Inventory::func(int item_)
{
	switch (item_)
	{
	case GEO_SWORD:
		if (item[GEO_SWORD] != NULL)
			item[GEO_SWORD]->func();
		break;
	case GEO_SPEAR:
		if (item[GEO_SPEAR] != NULL)
			item[GEO_SPEAR]->func();
		break;
	case GEO_BOW:
		if (item[GEO_BOW] != NULL)
			item[GEO_BOW]->func();
		
		break;
	case GEO_ARROW:
		if (item[GEO_ARROW] != NULL)
			item[GEO_ARROW]->func();
		else
			item[GEO_ARROW]->func();
		break;
	default:
		break;
	}
}

void Inventory::update(double dt)
{
	if (item[GEO_SWORD]->state == Items::front)
	{
		if (rotate < 0.08)
			rotate += dt;
		else
			item[GEO_SWORD]->state = Items::back;
	}
	if (item[GEO_SWORD]->state == Items::back)
	{
		if (rotate > 0.009)
			rotate -= dt;
		else
			item[GEO_SWORD]->state = Items::idle;
	}
}











//----------Items-----------
Items::Items()
{
}
void Items::melee()
{
	if (state == idle)
	{
		state = front;
	}
	
}
void Items::shoot()
{
}
Items::~Items()
{
}

//Spear
Spear::Spear()
{
	amount = 1;
	state = idle;
}
void Spear::func()
{
	melee();
}
Spear::~Spear()
{
}

//Bow
Bow::Bow()
{
	amount = 1;
	state = idle;
}
void Bow::func()
{
	shoot();
	std::cout << "ahahahahahaa" << std::endl;
}
Bow::~Bow()
{
}

//Arrow
Arrow::Arrow()
{
	amount = 5;
	state = idle;
}
void Arrow::func()
{
	amount += 5;
}
Arrow::~Arrow()
{
}