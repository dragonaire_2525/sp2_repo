#ifndef BOX_H
#define BOX_H
#include "Vector3.h"
#include "Test.h"
struct Box
{
	Vector3 position;
	Vector3 size;

	float minX;
	float maxX;

	float minY;
	float maxY;

	float minZ;
	float maxZ;

	bool rt;

	Box(Vector3 _position, Vector3 _size)
	{
		rt = false;
		size = _size;
		position = _position;
		minX = position.x - _size.x;
		maxX = position.x + _size.x;
		minY = position.y - _size.y;
		maxY = position.y + _size.y;
		minZ = position.z - _size.z;
		maxZ = position.z + _size.z;
	}

	bool inrange(Vector3 _position)
	{
		Vector3 pos = _position;
		if (((pos.x <= maxX + 2.5f) && (pos.x >= minX - 2.5f)) && ((pos.y <= maxY + 2.5f) && (pos.y >= minY - 2.5f)) && ((pos.z <= maxZ + 2.5f) && (pos.z >= minZ - 2.5f)))
		{
			return true;
		}
		else return false;
	}
	void move(Vector3 _position)
	{
		position = _position;
		minX = position.x - size.x;
		maxX = position.x + size.x;
		minY = position.y - size.y;
		maxY = position.y + size.y;
		minZ = position.z - size.z;
		maxZ = position.z + size.z;
	}
	funcx dofunction;
};
#endif
