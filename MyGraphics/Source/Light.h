#ifndef LIGHT_H
#define LIGHT_H

#include "Scene.h"
#include "Mesh.h"
#include "MeshBuilder.h"
#include "MatrixStack.h"

struct Light
{
	//add these after existing parameters
	enum LIGHT_TYPE
	{
		LIGHT_POINT = 0,
		LIGHT_DIRECTIONAL,
		LIGHT_SPOT,
	};
	LIGHT_TYPE type;
	Vector3 spotDirection;
	float cosCutoff;
	float cosInner;
	float exponent;

	Position position;
	Color color;
	float power;
	float kC, kL, kQ;

	Light()
	{
		color.Set(1, 1, 1);
		power = 1.0f;
		kC = 1.0f;
		kL = 0.0f;
		kQ = 0.0f;
	}

};

#endif