#ifndef SCENE_H
#define SCENE_H
#include "Inventory.h"

class Scene
{
public:
	Scene() {}
	~Scene() {}
	static enum GEOMETRY_TYPE
	{
		GEO_CYLINDER,
		GEO_SWORD,
		GEO_SPEAR,
		GEO_BOW,
		GEO_ARROW,
		GEO_DINO,
		GEO_DINOLEG,
		GEO_JAW,
		GEO_ENTRY,
		GEO_HOLE,
		GEO_TREE,
		GEO_WATER,
		GEO_AXES,
		GEO_QUAD,
		GEO_CUBE,
		GEO_CIRCLE,
		GEO_RING,
		GEO_SPHERE,
		GEO_LIGHTBALL,
		GEO_LIGHTBALL2,
		GEO_LEFT,
		GEO_RIGHT,
		GEO_TOP,
		GEO_BOTTOM,
		GEO_FRONT,
		GEO_BACK,
		GEO_TEXT,
		GEO_GROUND,
		GEO_UI,
		GEO_HAND,
		GEO_HEALTH,
		GEO_STAMINA,
		GEO_LADDER,
		GEO_LEDGE,
		NUM_GEOMETRY,
	};
	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Render() = 0;
	virtual bool ChangeScene() = 0;
	virtual void Exit() = 0;
};

#endif