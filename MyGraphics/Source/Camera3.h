#ifndef CAMERA_3_H
#define CAMERA_3_H

#include "Camera.h"

class Camera3 : public Camera
{
public:
	//Vector3 position;
	//Vector3 target;
	//Vector3 up;

	Vector3 defaultPosition;
	Vector3 defaultTarget;
	Vector3 defaultUp;

	Vector3 view;
	Vector3 right;
	Vector3 move;
	Vector3 side;

	Camera3();
	~Camera3();
	virtual void Init(const Vector3& pos, const Vector3& target, const Vector3& up);
	virtual void Update(double dt);
	virtual void Reset();
	virtual void recoverStamina();
	double xMouse;
	double yMouse;

	///////////
	float totalyaw;
	float totalpitch;
	//////
	float staminaPercentage;
	bool movementInput[4];
	bool exhaust;
	bool exhaustCheck;
	bool moveCheck();

	bool jump;
};

#endif