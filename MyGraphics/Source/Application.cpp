
#include "Application.h"

//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

//Include the standard C++ headers
#include <stdio.h>
#include <stdlib.h>

#include "SP2Scene.h"
#include "Cave.h"
Inventory* Inventory::invinstance = 0;
GLFWwindow* m_window;
const unsigned char FPS = 60; // FPS of this game
const unsigned int frameTime = 1000 / FPS; // time for each frame
//Define an error callback
static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
	_fgetchar();
}

//Define the key input callback
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}

bool Application::IsKeyPressed(unsigned short key)
{
    return ((GetAsyncKeyState(key) & 0x8001) != 0);
}
bool Application::IsPressedOnce(unsigned short key)
{
	static int state = GLFW_RELEASE;
	int keystate = glfwGetKey(m_window, key);
	if (state == GLFW_RELEASE && keystate == GLFW_PRESS)
	{
		state = keystate;
		return true;
	}
	state = keystate;
	return false;
}
bool Application::IsMousePressed()
{
	static int oldstate = GLFW_RELEASE;
	int mousestate = glfwGetMouseButton(m_window, GLFW_MOUSE_BUTTON_LEFT);
	if (oldstate == GLFW_RELEASE && mousestate == GLFW_PRESS)
	{
		oldstate = mousestate;
		return true;
	}
	oldstate = mousestate;
	return false;
}
static int scrollamt = 0;
static int difference = 0;
void scrollfunc(GLFWwindow*,double,double);
void scrollfunc(GLFWwindow* m_window, double x, double y)
{
	scrollamt = y;
}
int Application::MouseScroll()
{
	glfwSetScrollCallback(m_window, scrollfunc);
	return scrollamt;	
}
void Application::ResetScroll()
{
	scrollamt = 0;
}
void Application::GetMousePos(double &xpos, double &ypos)
{
	glfwGetCursorPos(m_window, &xpos, &ypos);
	//Hides the mouse
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
}
void Application::SetMousePos()
{
	glfwSetCursorPos(m_window, 400, 300);
}

Application::Application()
{
}

Application::~Application()
{
}

void resize_callback(GLFWwindow* window, int w, int h)
{
	glViewport(0, 0, w, h); //update opengl the new window size
}


void Application::Init()
{
	//Set the error callback
	glfwSetErrorCallback(error_callback);

	//Initialize GLFW
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	//Set the GLFW window creation hints - these are optional
	glfwWindowHint(GLFW_SAMPLES, 4); //Request 4x antialiasing
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //Request a specific OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //Request a specific OpenGL version
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 


	//Create a window and create its OpenGL context
	m_window = glfwCreateWindow(800, 600, "Computer Graphics", NULL, NULL);
	glfwSetWindowSizeCallback(m_window, resize_callback);
	//If the window couldn't be created
	if (!m_window)
	{
		fprintf( stderr, "Failed to open GLFW window.\n" );
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	//This function makes the context of the specified window current on the calling thread. 
	glfwMakeContextCurrent(m_window);

	//Sets the key callback
	glfwSetKeyCallback(m_window, key_callback);

	glewExperimental = true; // Needed for core profile
	//Initialize GLEW
	GLenum err = glewInit();

	//If GLEW hasn't initialized
	if (err != GLEW_OK) 
	{
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
		//return -1;
	}
	glfwSetTime(1.0);
}

void Application::Run()
{
	//Main Loop
	Scene *scene1 = new SP2Scene();
	Scene *scene2 = new Cave();
	Scene *scene = scene1;
	scene->Init();
	scene2->Init();



	m_timer.startTimer();    // Start timer to calculate how long it takes to render this frame
	while (!glfwWindowShouldClose(m_window) && !IsKeyPressed(VK_ESCAPE))
	{
		scene->Update(m_timer.getElapsedTime());
		scene->Render();
		//Swap buffers
		glfwSwapBuffers(m_window);
		//Get and organize events, like keyboard and mouse input, window resizing, etc...
		glfwPollEvents();
        m_timer.waitUntil(frameTime);       // Frame rate limiter. Limits each frame to a specified time in ms.   

		if (scene->ChangeScene())
		{
			if (scene == scene1)
			{
				scene = scene2;
			}
			else
			{
				scene = scene1;
			}
		}

	} //Check if the ESC key had been pressed or if the window had been closed
	scene->Exit();
	scene2->Exit();
	delete scene1;
	delete scene2;

}

void Application::Exit()
{
	//Close OpenGL window and terminate GLFW
	glfwDestroyWindow(m_window);
	//Finalize and clean up GLFW
	glfwTerminate();
}
