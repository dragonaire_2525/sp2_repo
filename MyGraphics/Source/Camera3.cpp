#include "Camera3.h"
#include "Application.h"
#include "Mtx44.h"
#include <math.h>

Camera3::Camera3()
{
	xMouse=0;
	yMouse=0;
}

Camera3::~Camera3()
{
}

void Camera3::Init(const Vector3& pos, const Vector3& target, const Vector3& up)
{
	this->position = defaultPosition = pos;
	this->target = defaultTarget = target;
	view = (target - position).Normalized();
	right = view.Cross(up);
	right.y = 0;
	right.Normalize();
	move = view;
    side = right;
	side.y = 0;
	this->up = defaultUp = right.Cross(view).Normalized();
	xMouse, yMouse = 0;
	staminaPercentage = 100.f;
	totalyaw = 0;
	totalpitch = 0;
	for (int i = 0; i < 4; i++)
	{
		movementInput[i] = false;
	}
}

void Camera3::Update(double dt)
{	
	view = (target - position).Normalized();
	right = view.Cross(up);
	move = view;
	move.y = 0;
	side = right;
	side.y = 0;
	static const float CAMERA_SPEED = 55.f;
	static float MOVEMENT_SPEED = 0.5f;
	if (exhaust == false)
	{
		if (Application::IsKeyPressed(VK_SHIFT))
		{
			MOVEMENT_SPEED = 1.f;
		}
		else
		{
			MOVEMENT_SPEED = 0.5f;
		}
	}
	else
	{
		MOVEMENT_SPEED = 0.0f;
	}
	if (exhaust == false)
	{
		if (Application::IsKeyPressed('A'))
		{
			position = position - side* MOVEMENT_SPEED;
			target = position + view;
			movementInput[0] = true;
		}
		else
		{
			movementInput[0] = false;
		}
		if (Application::IsKeyPressed('D'))
		{
			position = position + side* MOVEMENT_SPEED;
			target = position + view;
			movementInput[1] = true;
		}
		else
		{
			movementInput[1] = false;
		}
		if (Application::IsKeyPressed('W'))
		{
			position = position + move* MOVEMENT_SPEED;
			target = position + view;
			movementInput[2] = true;
		}
		else
		{
			movementInput[2] = false;
		}
		if (Application::IsKeyPressed('S'))
		{
			position = position - move* MOVEMENT_SPEED;
			target = position + view;
			movementInput[3] = true;
		}
		else
		{
			movementInput[3] = false;
		}
	}
	if (Application::IsKeyPressed(VK_SPACE))
	{
		position = position + up;
		target = position + view; 
	}
	if (Application::IsKeyPressed('N'))
	{
		position = position + view* MOVEMENT_SPEED;
		target = position + view;
	}
	if (Application::IsKeyPressed('M'))
	{
		position = position - view* MOVEMENT_SPEED;
		target = position + view;
	}
	//arrow keys
	/*
	if (Application::IsKeyPressed(VK_LEFT))
	{
		float yaw = (float)(+CAMERA_SPEED * dt);
		totalyaw += yaw;
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);


		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_RIGHT))
	{
		float yaw = (float)(-CAMERA_SPEED * dt);
		totalyaw += yaw;
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		Mtx44 rotation;
		rotation.SetToRotation(yaw, up.x, up.y, up.z);


		view = rotation * view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_UP))
	{
		float pitch = (float)(CAMERA_SPEED * dt);
		totalpitch += pitch;
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(pitch, right.x, right.y, right.z);

		view = rotation*view;
		target = position + view;
	}
	if (Application::IsKeyPressed(VK_DOWN))
	{
		float pitch = (float)(-CAMERA_SPEED * dt);
		totalpitch += pitch;
		Vector3 view = (target - position).Normalized();
		Vector3 right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(pitch, right.x, right.y, right.z);
		view = rotation*view;
		target = position + view;
	}
	*/
	Application::GetMousePos(xMouse, yMouse);
	/*
	if (yMouse > 300)
	{
	
		float pitch = (float)(CAMERA_SPEED * dt * (yMouse - 300) * 0.1f);
		totalpitch += pitch;
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		if (totalpitch <= 90)
		{
			Mtx44 rotation;
			rotation.SetToRotation(-pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
			up = rotation * up;
		}
		else
		{
			totalpitch -= pitch;
		}
	}
	
	if (yMouse < 300)
	{
		float pitch = (float)(-CAMERA_SPEED * dt * (yMouse - 300) * 0.1f);
		totalpitch -= pitch;
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		if (totalpitch >= -90)
		{
			Mtx44 rotation;
			rotation.SetToRotation(pitch, right.x, right.y, right.z);
			view = rotation * view;
			target = position + view;
			up = rotation * up;
		}
		else
		{
			totalpitch += pitch;
		}	
	}

	if (xMouse > 400)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (xMouse - 400) * 0.1f);
		totalyaw += yaw;
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(-yaw, 0, 1, 0);
		view = rotation * view;
		target = position + view;
		up = rotation * up;
	}
	if (xMouse < 400)
	{
		float yaw = (float)(CAMERA_SPEED * dt * (xMouse - 400) * 0.1f);
		totalyaw += yaw;
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		Mtx44 rotation;
		rotation.SetToRotation(-yaw, 0, 1, 0);
		view = rotation * view;
		target = position + view;
		up = rotation * up;
	}
	*/
	//mouse movement
	if ((xMouse < 800 && xMouse > 0) && (yMouse < 600 && yMouse > 0)) //if mouse is inside of console
	{
		float yaw = (float)(CAMERA_SPEED * dt * (float)(400 - xMouse));
		totalyaw += yaw;
		Mtx44 rotation;
		view = (target - position).Normalized();
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();
		rotation.SetToRotation(yaw, 0, 1, 0);
		view = rotation * view;
		target = position + view;
		up = rotation * up;

		float pitch = (float)(CAMERA_SPEED * dt * (float)(300 - yMouse));
		totalpitch += pitch;
		right = view.Cross(up);
		right.y = 0;
		right.Normalize();
		up = right.Cross(view).Normalized();

		if (totalpitch <= 85 && totalpitch >= -45)
		{
			Mtx44 rotation;
			rotation.SetToRotation(pitch, right.x, right.y, right.z);

			view = rotation * view;
			target = position + view;
			up = rotation * up;
		}
		else
		{
			totalpitch -= pitch;
		}
	}
	
	if (Application::IsKeyPressed(VK_SHIFT))
	{
		if (exhaust == false)
		{
			if (moveCheck() == true)
			{
				if (staminaPercentage > 0)
					staminaPercentage--;
				else
					exhaust = true;
			}
			else
			{
				recoverStamina();
			}
		}
		else
		{
			recoverStamina();
		}
	}
	else
	{
		recoverStamina();
	}
	Application::SetMousePos();
	if (Application::IsKeyPressed('R'))
	{
		Reset();
	}
}

void Camera3::Reset()
{
	position = defaultPosition;
	target = defaultTarget;
	up = defaultUp;
}

bool Camera3::moveCheck()
{
	for (int m = 0; m < 4; m++)
	{
		if (movementInput[m] == true)
			return true;
	}
	return false;
}

void Camera3::recoverStamina()
{
	if (staminaPercentage < 100)
		staminaPercentage++;
	else
		exhaust = false;
}