#ifndef AI_H
#define AI_H
#include <iostream>
#include "Vector3.h"
#include "Test.h"
#include <vector> 

class AI
{
public:
	Vector3 position;
	Vector3 origin;
	Vector3 size;
	/*std::vector<Vector3*> Waypoints;
	std::vector<Vector3*> Directions;*/
	std::vector<Vector3> Waypoints;
	std::vector<Vector3> Directions;
	bool DirCheck = true;
	Vector3 maginitude;
	//For collision
	float minX;
	float maxX;
	float minY;
	float maxY;
	float minZ;
	float maxZ;
	int WayCount = 0;
	int Way = 1;

	//Setting the Waypoints
	/*void SetWayPoint(Vector3* pos);
	void SetDirection(Vector3* dir);*/
	void SetWayPoint(Vector3 pos);
	void SetDirection(Vector3 dir);

	//Chasing range
	float range;
	float o_range;

	//inside AITYPES
	float spd;
	void move(Vector3);

	AI(Vector3, float, Vector3);

	void inrange(Vector3 _position); //to be moved into AITYPES
	~AI();
private:
	bool inrangeb;
	bool check;
	//AITYPES* AIType; For different types of AI
};
#endif