#include "Cave.h"
#include "GL\glew.h"

#include "shader.hpp"
#include "Mtx44.h"
#include "Application.h"
#include "Utility.h"
#include "LoadTGA.h"
Cave::Cave()
{
	inventory = Inventory::getInstance();
}

Cave::~Cave()
{
}

void Cave::Init()
{
	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	// Generate a default VAO for now
	glGenVertexArrays(1, &m_vertexArrayID);
	glBindVertexArray(m_vertexArrayID);
	glDisable(GL_CULL_FACE);

//	camera.Init(Vector3(-485, 1, 0), Vector3(0, 0, 0), Vector3(0, 1, 0));
	camera.Init(Vector3(30, 1, 30), Vector3(0, 0, 0), Vector3(0, 1, 0));
	tmpCam = camera;

	Mtx44 projection;
	projection.SetToPerspective(45.f, 4.f / 3.f, 0.1f, 10000.f);
	projectionStack.LoadMatrix(projection);


	rotateAngle = 0.0f;
	translateX = 0.0f;
	scaleAll = 0.0f;

	rotateAmt = 60;
	translateAmt = 10;
	scaleAmt = 2;
	//Load vertex and fragment shaders
	//m_programID = LoadShaders("Shader//TransformVertexShader.vertexshader", "Shader//SimpleFragmentShader.fragmentshader");
	//m_programID = LoadShaders("Shader//Shading.vertexshader", "Shader//Shading.fragmentshader");
	//m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Texture.fragmentshader");
	//m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Blending.fragmentshader");
	m_programID = LoadShaders("Shader//Texture.vertexshader", "Shader//Text.fragmentshader");

	m_parameters[U_MVP] = glGetUniformLocation(m_programID, "MVP");
	m_parameters[U_MODELVIEW] = glGetUniformLocation(m_programID, "MV");
	m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE] = glGetUniformLocation(m_programID, "MV_inverse_transpose");
	m_parameters[U_MATERIAL_AMBIENT] = glGetUniformLocation(m_programID, "material.kAmbient");
	m_parameters[U_MATERIAL_DIFFUSE] = glGetUniformLocation(m_programID, "material.kDiffuse");
	m_parameters[U_MATERIAL_SPECULAR] = glGetUniformLocation(m_programID, "material.kSpecular");
	m_parameters[U_MATERIAL_SHININESS] = glGetUniformLocation(m_programID, "material.kShininess");
	m_parameters[U_LIGHT0_POSITION] = glGetUniformLocation(m_programID, "lights[0].position_cameraspace");
	m_parameters[U_LIGHT0_COLOR] = glGetUniformLocation(m_programID, "lights[0].color");
	m_parameters[U_LIGHT0_POWER] = glGetUniformLocation(m_programID, "lights[0].power");
	m_parameters[U_LIGHT0_KC] = glGetUniformLocation(m_programID, "lights[0].kC");
	m_parameters[U_LIGHT0_KL] = glGetUniformLocation(m_programID, "lights[0].kL");
	m_parameters[U_LIGHT0_KQ] = glGetUniformLocation(m_programID, "lights[0].kQ");
	m_parameters[U_LIGHT1_POSITION] = glGetUniformLocation(m_programID, "lights[1].position_cameraspace");
	m_parameters[U_LIGHT1_COLOR] = glGetUniformLocation(m_programID, "lights[1].color");
	m_parameters[U_LIGHT1_POWER] = glGetUniformLocation(m_programID, "lights[1].power");
	m_parameters[U_LIGHT1_KC] = glGetUniformLocation(m_programID, "lights[1].kC");
	m_parameters[U_LIGHT1_KL] = glGetUniformLocation(m_programID, "lights[1].kL");
	m_parameters[U_LIGHT1_KQ] = glGetUniformLocation(m_programID, "lights[1].kQ");
	m_parameters[U_LIGHT2_POSITION] = glGetUniformLocation(m_programID, "lights[2].position_cameraspace");
	m_parameters[U_LIGHT2_COLOR] = glGetUniformLocation(m_programID, "lights[2].color");
	m_parameters[U_LIGHT2_POWER] = glGetUniformLocation(m_programID, "lights[2].power");
	m_parameters[U_LIGHT2_KC] = glGetUniformLocation(m_programID, "lights[2].kC");
	m_parameters[U_LIGHT2_KL] = glGetUniformLocation(m_programID, "lights[2].kL");
	m_parameters[U_LIGHT2_KQ] = glGetUniformLocation(m_programID, "lights[2].kQ");
	m_parameters[U_LIGHT3_POSITION] = glGetUniformLocation(m_programID, "lights[3].position_cameraspace");
	m_parameters[U_LIGHT3_COLOR] = glGetUniformLocation(m_programID, "lights[3].color");
	m_parameters[U_LIGHT3_POWER] = glGetUniformLocation(m_programID, "lights[3].power");
	m_parameters[U_LIGHT3_KC] = glGetUniformLocation(m_programID, "lights[3].kC");
	m_parameters[U_LIGHT3_KL] = glGetUniformLocation(m_programID, "lights[3].kL");
	m_parameters[U_LIGHT3_KQ] = glGetUniformLocation(m_programID, "lights[3].kQ");

	m_parameters[U_LIGHT4_POSITION] = glGetUniformLocation(m_programID, "lights[4].position_cameraspace");
	m_parameters[U_LIGHT4_COLOR] = glGetUniformLocation(m_programID, "lights[4].color");
	m_parameters[U_LIGHT4_POWER] = glGetUniformLocation(m_programID, "lights[4].power");
	m_parameters[U_LIGHT4_KC] = glGetUniformLocation(m_programID, "lights[4].kC");
	m_parameters[U_LIGHT4_KL] = glGetUniformLocation(m_programID, "lights[4].kL");
	m_parameters[U_LIGHT4_KQ] = glGetUniformLocation(m_programID, "lights[4].kQ");
	m_parameters[U_LIGHT4_TYPE] = glGetUniformLocation(m_programID, "lights[4].type");
	m_parameters[U_LIGHT4_SPOTDIRECTION] = glGetUniformLocation(m_programID, "lights[4].spotDirection");
	m_parameters[U_LIGHT4_COSCUTOFF] = glGetUniformLocation(m_programID, "lights[4].cosCutoff");
	m_parameters[U_LIGHT4_COSINNER] = glGetUniformLocation(m_programID, "lights[4].cosInner");
	m_parameters[U_LIGHT4_EXPONENT] = glGetUniformLocation(m_programID, "lights[4].exponent");

	m_parameters[U_LIGHTENABLED] = glGetUniformLocation(m_programID, "lightEnabled");
	m_parameters[U_NUMLIGHTS] = glGetUniformLocation(m_programID, "numLights");

	m_parameters[U_COLOR_TEXTURE_ENABLED] = glGetUniformLocation(m_programID, "colorTextureEnabled");
	m_parameters[U_COLOR_TEXTURE] = glGetUniformLocation(m_programID, "colorTexture");
	m_parameters[U_TEXT_ENABLED] = glGetUniformLocation(m_programID, "textEnabled");
	m_parameters[U_TEXT_COLOR] = glGetUniformLocation(m_programID, "textColor");

	glUseProgram(m_programID);

	//light[0].position.Set(0, 20, 0);
	//light[0].color.Set(1, 1, 1);
	//light[0].power = 1000;
	//light[0].kC = 1.f;
	//light[0].kL = 0.01f;
	//light[0].kQ = 0.001f;


	//// Make sure you pass uniform parameters after glUseProgram()
	//glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	//glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	//glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	//glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	//glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);


	//light[1].position.Set(20, 20, 0);
	//light[1].color.Set(1, 1, 1);
	//light[1].power = 1;
	//light[1].kC = 1.f;
	//light[1].kL = 0.01f;
	//light[1].kQ = 0.001f;

	//glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	//glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	//glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	//glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	//glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);



	light[2].position.Set(20, 20, 0);
	light[2].color.Set(1, 1, 1);
	light[2].power = 1;
	light[2].kC = 1.f;
	light[2].kL = 0.01f;
	light[2].kQ = 0.001f;

	glUniform3fv(m_parameters[U_LIGHT2_COLOR], 1, &light[1].color.r);
	glUniform1f(m_parameters[U_LIGHT2_POWER], light[1].power);
	glUniform1f(m_parameters[U_LIGHT2_KC], light[1].kC);
	glUniform1f(m_parameters[U_LIGHT2_KL], light[1].kL);
	glUniform1f(m_parameters[U_LIGHT2_KQ], light[1].kQ);
	glUniform1i(m_parameters[U_NUMLIGHTS], 3);


	//light[0].type = Light::LIGHT_SPOT;
	//light[0].position.Set(313, -179, -285);
	//light[0].color.Set(1, 1, 1);
	//light[0].power = 100;
	//light[0].kC = 1.f;
	//light[0].kL = 0.01f;
	//light[0].kQ = 0.001f;
	//light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	//light[0].cosInner = cos(Math::DegreeToRadian(30));
	//light[0].exponent = 3.f;
	//light[0].spotDirection.Set(0.f, 1.f, 0.f);


	//// Make sure you pass uniform parameters after glUseProgram()
	//glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	//glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	//glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	//glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	//glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	//glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	//glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	//glUniform1f(m_parameters[U_LIGHT0_COSINNER], light[0].cosInner);
	//glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);

	//light[1].type = Light::LIGHT_SPOT;
	//light[1].position.Set(313, 179, -256);
	//light[1].color.Set(1, 1, 1);
	//light[1].power = 3;
	//light[1].kC = 1.f;
	//light[1].kL = 0.01f;
	//light[1].kQ = 0.001f;
	//light[1].cosCutoff = cos(Math::DegreeToRadian(45));
	//light[1].cosInner = cos(Math::DegreeToRadian(30));
	//light[1].exponent = 3.f;
	//light[1].spotDirection.Set(-1.f, 0.f, 0.f);
	//glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);
	//glUniform3fv(m_parameters[U_LIGHT1_COLOR], 1, &light[1].color.r);
	//glUniform1f(m_parameters[U_LIGHT1_POWER], light[1].power);
	//glUniform1f(m_parameters[U_LIGHT1_KC], light[1].kC);
	//glUniform1f(m_parameters[U_LIGHT1_KL], light[1].kL);
	//glUniform1f(m_parameters[U_LIGHT1_KQ], light[1].kQ);
	//glUniform1f(m_parameters[U_LIGHT1_COSCUTOFF], light[1].cosCutoff);
	//glUniform1f(m_parameters[U_LIGHT1_COSINNER], light[1].cosInner);
	//glUniform1f(m_parameters[U_LIGHT1_EXPONENT], light[1].exponent);

	light[2].type = Light::LIGHT_POINT;
	light[2].position.Set(0, 400, 0);
	light[2].color.Set(1, 1, 1);
	light[2].power = 500;
	light[2].kC = 1.f;
	light[2].kL = 0.01f;
	light[2].kQ = 0.001f;
	light[2].cosCutoff = cos(Math::DegreeToRadian(45));
	light[2].cosInner = cos(Math::DegreeToRadian(30));
	light[2].exponent = 3.f;
	//light[2].spotDirection.Set(-1.f, 0.f, 0.f);
	glUniform1i(m_parameters[U_LIGHT2_TYPE], light[2].type);
	glUniform3fv(m_parameters[U_LIGHT2_COLOR], 1, &light[2].color.r);
	glUniform1f(m_parameters[U_LIGHT2_POWER], light[2].power);
	glUniform1f(m_parameters[U_LIGHT2_KC], light[2].kC);
	glUniform1f(m_parameters[U_LIGHT2_KL], light[2].kL);
	glUniform1f(m_parameters[U_LIGHT2_KQ], light[2].kQ);
	glUniform1f(m_parameters[U_LIGHT2_COSCUTOFF], light[2].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT2_COSINNER], light[2].cosInner);
	glUniform1f(m_parameters[U_LIGHT2_EXPONENT], light[2].exponent);

	// Use our shader
	glUseProgram(m_programID);
	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		meshList[i] = NULL;
		objects[i] = NULL;
		aitypes[i] = NULL;
		interactobj[i] = NULL;
	}

	Vector3 pos(0, 0, 0);

	meshList[GEO_FRONT] = MeshBuilder::GenerateQuad("front", Color(1, 1, 1), 1000.0f, 1000.0f);
	objects[GEO_FRONT] = new Box(Vector3(0, 0, 499), Vector3(500, 500, 1));
	objects[GEO_BACK] = new Box(Vector3(0, 0, -499), Vector3(500, 500, 1));
	meshList[GEO_BACK] = MeshBuilder::GenerateQuad("back", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_BACK]->textureID = LoadTGA("Image//Cave.tga");
	meshList[GEO_FRONT]->textureID = LoadTGA("Image//Cave.tga");
	meshList[GEO_LEFT] = MeshBuilder::GenerateQuad("left", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_LEFT]->textureID = LoadTGA("Image//Cave.tga");
	objects[GEO_LEFT] = new Box(Vector3(-499, 0, 0), Vector3(1, 500, 500));
	meshList[GEO_RIGHT] = MeshBuilder::GenerateQuad("right", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_RIGHT]->textureID = LoadTGA("Image//Cave.tga");
	objects[GEO_RIGHT] = new Box(Vector3(499, 0, 0), Vector3(1, 500, 500));
	meshList[GEO_TOP] = MeshBuilder::GenerateQuad("top", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_TOP]->textureID = LoadTGA("Image//Cave.tga");
	meshList[GEO_BOTTOM] = MeshBuilder::GenerateQuad("bottom", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_BOTTOM]->textureID = LoadTGA("Image//Cave.tga");
	meshList[GEO_GROUND] = MeshBuilder::GenerateQuad("Ground", Color(1, 1, 1), 1000.0f, 1000.0f);
	meshList[GEO_GROUND]->textureID = LoadTGA("Image//Cave.tga");
	objects[GEO_GROUND] = new Box(Vector3(0, -1, 0), Vector3(500, 1, 500));

	meshList[GEO_LEDGE] = MeshBuilder::GenerateQuad("Ledge", Color(1, 1, 1), 1000.0f, 10.0f);
	meshList[GEO_LEDGE]->textureID = LoadTGA("Image//Ledge.tga");
	objects[GEO_LEDGE] = new Box(Vector3(0, 2, 375), Vector3(500, 8, 125));

	meshList[GEO_SWORD] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Sword.obj");
	meshList[GEO_SWORD]->textureID = LoadTGA("Image//EXUCA.tga");

	meshList[GEO_ENTRY] = MeshBuilder::GenerateQuad("Entrance", Color(1, 1, 1), 10.0f, 10.0f);
	meshList[GEO_ENTRY]->textureID = LoadTGA("Image//Cave.tga");
	objects[GEO_ENTRY] = new Box(Vector3(-495, 5, 0), Vector3(6, 5, 6));

	meshList[GEO_HOLE] = MeshBuilder::GenerateQuad("EntranceHole", Color(1, 1, 1), 10.0f, 10.0f);
	meshList[GEO_HOLE]->textureID = LoadTGA("Image//Entrance.tga");

	//meshList[GEO_CYLINDER] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Body.obj");
	//meshList[GEO_CYLINDER]->textureID = LoadTGA("Image//BodyUV.tga");
	//aitypes[GEO_CYLINDER] = new AI(Vector3(0, 0, 0), 20.f, Vector3(1, 1, 1));


	meshList[GEO_SPEAR] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Spear.obj");
	meshList[GEO_SPEAR]->textureID = LoadTGA("Image//SpearUV.tga");

	meshList[GEO_BOW] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Bow.obj");
	meshList[GEO_BOW]->textureID = LoadTGA("Image//BowUV.tga");

	meshList[GEO_ARROW] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Arrow.obj");
	meshList[GEO_ARROW]->textureID = LoadTGA("Image//ArrowUV.tga");

	meshList[GEO_LADDER] = MeshBuilder::GenerateOBJ("cylinder", "OBJ/Ladder.obj");
	meshList[GEO_LADDER]->textureID = LoadTGA("Image//LadderUV.tga");

	meshList[GEO_AXES] = MeshBuilder::GenerateAxes("reference", 1000, 1000, 1000);

	meshList[GEO_TEXT] = MeshBuilder::GenerateText("text", 16, 16);
	meshList[GEO_TEXT]->textureID = LoadTGA("Image//calibri.tga");

	meshList[GEO_UI] = MeshBuilder::GenerateQuad("User Interface", Color(1, 1, 1), 10.0f, 10.0f);
	meshList[GEO_UI]->textureID = LoadTGA("Image//UI.tga");

	meshList[GEO_LIGHTBALL] = MeshBuilder::GenerateSphere("lightball", Color(1, 1, 1), 9, 36, 2);
	meshList[GEO_LIGHTBALL2] = MeshBuilder::GenerateSphere("lightball", Color(1, 1, 1), 9, 36, 2);
	seconds = 0.0;
	meshList[GEO_QUAD] = MeshBuilder::GenerateQuad("sphere1", Color(1, 0, 0), 10.0f, 10.0f);
	inventory = Inventory::getInstance();
	exittext = false;
}

void Cave::Update(double dt)
{
	const float LSPEED = 10.0f;
	if (Application::IsKeyPressed('1'))
	{
		glEnable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('2'))
	{
		glDisable(GL_CULL_FACE);
	}
	if (Application::IsKeyPressed('3'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	if (Application::IsKeyPressed('4'))
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	if (Application::IsKeyPressed(VK_SPACE))
	{
	}
	if (Application::IsKeyPressed('I'))
	{
		light[0].position.z -= (float)(LSPEED * dt);
		light[1].position.z -= (float)(LSPEED * dt);

	}
	if (Application::IsKeyPressed('K'))
	{
		light[0].position.z += (float)(LSPEED * dt);
		light[1].position.z += (float)(LSPEED * dt);

	}
	if (Application::IsKeyPressed('J'))
	{
		light[0].position.x -= (float)(LSPEED * dt);
		light[1].position.x -= (float)(LSPEED * dt);

	}
	if (Application::IsKeyPressed('L'))
	{
		light[0].position.x += (float)(LSPEED * dt);
		light[1].position.x += (float)(LSPEED * dt);

	}
	if (Application::IsKeyPressed('O'))
	{
		light[0].position.y -= (float)(LSPEED * dt);
		light[1].position.y -= (float)(LSPEED * dt);

	}
	if (Application::IsKeyPressed('P'))
	{
		light[0].position.y += (float)(LSPEED * dt);
		light[1].position.y += (float)(LSPEED * dt);
	}

	//to do: switch light type to SPOT and pass the information to shader
	/*light[0].position.Set(20, 20, 20);
	light[0].color.Set(1, 1, 1);
	light[0].power = 1000;
	light[0].kC = 1.f;
	light[0].kL = 0.01f;
	light[0].kQ = 0.001f;
	light[0].cosCutoff = cos(Math::DegreeToRadian(45));
	light[0].exponent = 3.f;
	light[0].spotDirection.Set(0.f, -1.f, 0.f);

	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);
	glUniform3fv(m_parameters[U_LIGHT0_COLOR], 1, &light[0].color.r);
	glUniform1f(m_parameters[U_LIGHT0_POWER], light[0].power);
	glUniform1f(m_parameters[U_LIGHT0_KC], light[0].kC);
	glUniform1f(m_parameters[U_LIGHT0_KL], light[0].kL);
	glUniform1f(m_parameters[U_LIGHT0_KQ], light[0].kQ);
	glUniform1f(m_parameters[U_LIGHT0_COSCUTOFF], light[0].cosCutoff);
	glUniform1f(m_parameters[U_LIGHT0_EXPONENT], light[0].exponent);*/

	tmpCam.position = camera.position;
	tmpCam.target = camera.target;
	camera.position += Vector3(0, -0.1f, 0);
	camera.target = camera.position + camera.view;
	if (Boundscheck(camera.position))
	{
		camera.position = tmpCam.position;
		camera.target = tmpCam.target;
	}

	tmpCam.position = camera.position;
	tmpCam.target = camera.target;
	camera.Update(dt);
	inventory->update(dt);
	if (Boundscheck(camera.position))
	{
		camera.position = tmpCam.position;
		camera.target = tmpCam.target;
	}
	Checkrange();
	//moving ai
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (aitypes[i] != NULL)
		{
			tmpCam.position = aitypes[i]->position;
			aitypes[i]->position += Vector3(0, -1.f, 0);
			if (Boundscheck(aitypes[i]->position))
			{
				aitypes[i]->move(tmpCam.position);
			}
		}
	}
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (aitypes[i] != NULL)
		{
			tmpCam.position = aitypes[i]->position;
			aitypes[i]->move(camera.position);
			if (Boundscheck(aitypes[i]->position))
			{
				aitypes[i]->move(tmpCam.position);
			}
		}
	}
	seconds += dt;
	if (Application::IsMousePressed())
	{
		inventory->func();
	}
	std::cout << camera.position << std::endl;
	//std::cout << aitypes[GEO_CYLINDER]->maxX << std::endl;
	//std::cout << aitypes[GEO_CYLINDER]->minX << std::endl;


	/*light[0].type = Light::LIGHT_SPOT;
	glUniform1i(m_parameters[U_LIGHT0_TYPE], light[0].type);




	light[1].type = Light::LIGHT_SPOT;
	glUniform1i(m_parameters[U_LIGHT1_TYPE], light[1].type);*/

	light[2].type = Light::LIGHT_POINT;
	glUniform1i(m_parameters[U_LIGHT2_TYPE], light[2].type);


}

void Cave::Render()
{
	//Clear color & depth buffer every frame
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	Mtx44 MVP;

	viewStack.LoadIdentity();
	viewStack.LookAt(camera.position.x, camera.position.y, camera.position.z, camera.target.x, camera.target.y, camera.target.z, camera.up.x, camera.up.y, camera.up.z);
	modelStack.LoadIdentity();

	//if (light[0].type == Light::LIGHT_DIRECTIONAL)
	//{
	//	Vector3 lightDir(light[0].position.x, light[0].position.y, light[0].position.z);
	//	Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
	//	glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightDirection_cameraspace.x);
	//}
	//else if (light[0].type == Light::LIGHT_SPOT)
	//{
	//	Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
	//	glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	//	Vector3 spotDirection_cameraspace = viewStack.Top() * light[0].spotDirection;
	//	glUniform3fv(m_parameters[U_LIGHT0_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);

	//}
	//else
	//{
	//	Position lightPosition_cameraspace = viewStack.Top() * light[0].position;
	//	glUniform3fv(m_parameters[U_LIGHT0_POSITION], 1, &lightPosition_cameraspace.x);
	//}


	//if (light[1].type == Light::LIGHT_DIRECTIONAL)
	//{
	//	Vector3 lightDir(light[1].position.x, light[1].position.y, light[1].position.z);
	//	Vector3 lightDirection_cameraspace2 = viewStack.Top() * lightDir;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightDirection_cameraspace2.x);
	//}
	//else if (light[1].type == Light::LIGHT_SPOT)
	//{
	//	Position lightPosition_cameraspace2 = viewStack.Top() * light[1].position;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace2.x);
	//	Vector3 spotDirection_cameraspace2 = viewStack.Top() * light[1].spotDirection;
	//	glUniform3fv(m_parameters[U_LIGHT1_SPOTDIRECTION], 1, &spotDirection_cameraspace2.x);
	//}
	//else
	//{
	//	Position lightPosition_cameraspace2 = viewStack.Top() * light[1].position;
	//	glUniform3fv(m_parameters[U_LIGHT1_POSITION], 1, &lightPosition_cameraspace2.x);
	//}




	if (light[2].type == Light::LIGHT_DIRECTIONAL)
	{
		Vector3 lightDir(light[2].position.x, light[2].position.y, light[2].position.z);
		Vector3 lightDirection_cameraspace = viewStack.Top() * lightDir;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightDirection_cameraspace.x);
	}
	else if (light[2].type == Light::LIGHT_SPOT)
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[2].position;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightPosition_cameraspace.x);
		Vector3 spotDirection_cameraspace = viewStack.Top() * light[2].spotDirection;
		glUniform3fv(m_parameters[U_LIGHT2_SPOTDIRECTION], 1, &spotDirection_cameraspace.x);

	}
	else
	{
		Position lightPosition_cameraspace = viewStack.Top() * light[2].position;
		glUniform3fv(m_parameters[U_LIGHT2_POSITION], 1, &lightPosition_cameraspace.x);
	}

	RenderSkybox();

	RenderMesh(meshList[GEO_AXES], false);

	modelStack.PushMatrix();
	modelStack.Rotate(-90, 1, 0, 0);
	modelStack.Translate(0, 0, -1);
	RenderMesh(meshList[GEO_GROUND], false);
	modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(aitypes[GEO_CYLINDER]->position.x, aitypes[GEO_CYLINDER]->position.y, aitypes[GEO_CYLINDER]->position.z);
	//RenderMesh(meshList[GEO_CYLINDER], false);
	//modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(light[0].position.x, light[0].position.y, light[0].position.z);
	//modelStack.Scale(1, 1, 1);
	//RenderMesh(meshList[GEO_LIGHTBALL], false);
	//modelStack.PopMatrix();

	//modelStack.PushMatrix();
	//modelStack.Translate(light[1].position.x, light[1].position.y, light[1].position.z);
	//modelStack.Scale(50, 50, 50);
	//RenderMesh(meshList[GEO_LIGHTBALL], false);
	//modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(camera.position.x + (camera.view.x * 3), camera.position.y + (camera.view.y * 3), camera.position.z + (camera.view.z * 3));
	modelStack.Rotate(-camera.totalpitch, -camera.right.x, -camera.right.y, -camera.right.z);
	modelStack.Rotate(camera.totalyaw, 0, 1, 0);
	modelStack.Translate(1.7f, -1, 1);
	modelStack.Rotate(-20, 0, 1, 0);
	modelStack.Rotate(180, 0, 0, 1);
	modelStack.Rotate(+inventory->rotate * 900, 0, 0, 1);
	modelStack.PushMatrix();
	modelStack.Translate(0, -1, 0);
	modelStack.Scale(0.1, 0.1, 0.1);
	RenderMesh(meshList[GEO_SWORD], false);
	modelStack.PopMatrix();
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(light[2].position.x, light[2].position.y, light[2].position.z);
	modelStack.Scale(50, 50, 50);
	RenderMesh(meshList[GEO_LIGHTBALL2], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, -0.9, 0);
	modelStack.Scale(0.5, 0.5, 0.5);
	RenderMesh(meshList[GEO_SPEAR], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(20, -0.9, 0);
	modelStack.Scale(0.1, 0.1, 0.1);
	RenderMesh(meshList[GEO_BOW], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(20, -0.9, 0);
	modelStack.Scale(0.1, 0.1, 0.1);
	RenderMesh(meshList[GEO_ARROW], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 1, 250);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_LADDER], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 4, 250);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_LADDER], false);
	modelStack.PopMatrix();


	modelStack.PushMatrix();
	modelStack.Translate(0, 2, 250);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_LEDGE], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(0, 7, 375);
	modelStack.Rotate(90, 1, 0, 0);
	modelStack.Scale(1, 25, 1);
	RenderMesh(meshList[GEO_LEDGE], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-495, 4, 5);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_ENTRY], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-495, 4, -5);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_ENTRY], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-495, 9, 0);
	modelStack.Rotate(90, 1, 0, 0);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_ENTRY], false);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	modelStack.Translate(-490, 4, 0);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Scale(1, 1, 1);
	RenderMesh(meshList[GEO_HOLE], false);
	modelStack.PopMatrix();

	if (exittext == true)
	{
		RenderTextOnScreen(meshList[GEO_TEXT], "Press E to exit cave", Color(1, 1, 1), 2, 1, 5);
	}

	RenderUserInterface();
	//RenderMeshOnScreen(meshList[GEO_QUAD], 10.0f, 10.0f, 10.0f, 10.0f);
}
bool Cave::ChangeScene()
{
	if ((camera.position.x> -500 && camera.position.x< -485) && (camera.position.z>-5 && camera.position.z<5))
	{
		exittext = true;
		if (Application::IsPressedOnce('E'))
		{
			return true;
		}
		else
			return false;
	}

	else
		exittext = false;
	return false;
}


void Cave::RenderMesh(Mesh * mesh, bool enableLight)
{
	Mtx44 MVP, modelView, modelView_inverse_transpose;


	MVP = projectionStack.Top() * viewStack.Top() *modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);
	modelView = viewStack.Top() * modelStack.Top();
	glUniformMatrix4fv(m_parameters[U_MODELVIEW], 1, GL_FALSE, &modelView.a[0]);
	if (enableLight)
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 1);
		modelView_inverse_transpose = modelView.GetInverse().GetTranspose();
		glUniformMatrix4fv(m_parameters[U_MODELVIEW_INVERSE_TRANSPOSE], 1, GL_FALSE, &modelView_inverse_transpose.a[0]);

		//load material
		glUniform3fv(m_parameters[U_MATERIAL_AMBIENT], 1, &mesh->material.kAmbient.r);
		glUniform3fv(m_parameters[U_MATERIAL_DIFFUSE], 1, &mesh->material.kDiffuse.r);
		glUniform3fv(m_parameters[U_MATERIAL_SPECULAR], 1, &mesh->material.kSpecular.r);
		glUniform1f(m_parameters[U_MATERIAL_SHININESS], mesh->material.kShininess);
	}
	else
	{
		glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	}

	if (mesh->textureID > 0)
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, mesh->textureID);
		glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	}
	else
	{
		glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 0);
	}
	mesh->Render(); //this line should only be called once 
	if (mesh->textureID > 0)
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}
}

void Cave::RenderSkybox()
{
	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 0, 499);
	modelStack.Rotate(180, 0, 1, 0);
	RenderMesh(meshList[GEO_FRONT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 0, -499);
	RenderMesh(meshList[GEO_BACK], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(-499, 0, 0);
	modelStack.Rotate(90, 0, 1, 0);
	RenderMesh(meshList[GEO_LEFT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(499, 0, 0);
	modelStack.Rotate(-90, 0, 1, 0);
	RenderMesh(meshList[GEO_RIGHT], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, 499, 0);
	modelStack.Rotate(-90, 0, 1, 0);
	modelStack.Rotate(90, 1, 0, 0);
	RenderMesh(meshList[GEO_TOP], true);
	modelStack.PopMatrix();

	modelStack.PushMatrix();
	//scale, translate, rotate
	modelStack.Translate(0, -499, 0);
	modelStack.Rotate(90, 0, 1, 0);
	modelStack.Rotate(-90, 1, 0, 0);
	RenderMesh(meshList[GEO_BOTTOM], true);
	modelStack.PopMatrix();
}
void Cave::RenderUserInterface()
{
	Vector3 pos;
	modelStack.PushMatrix();
	modelStack.Translate(-0.11, 0, 0);
	modelStack.PushMatrix();
	//scale, translate, rotate
	pos = camera.target - camera.position;
	modelStack.Translate(camera.position.x, camera.position.y, camera.position.z);
	modelStack.Translate(pos.x, pos.y, pos.z);
	modelStack.Scale(0.09, 0.09, 0.09);
	RenderMesh(meshList[GEO_UI], false);
	modelStack.PopMatrix();
}
void Cave::RenderText(Mesh* mesh, std::string text, Color color)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * 1.0f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);
	glEnable(GL_DEPTH_TEST);
}

void Cave::RenderTextOnScreen(Mesh* mesh, std::string text, Color color, float size, float x, float y)
{
	if (!mesh || mesh->textureID <= 0) //Proper error check
		return;

	glDisable(GL_DEPTH_TEST);

	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity(); //Reset modelStack
	modelStack.Scale(size, size, size);
	modelStack.Translate(x, y, 0);

	glUniform1i(m_parameters[U_TEXT_ENABLED], 1);
	glUniform3fv(m_parameters[U_TEXT_COLOR], 1, &color.r);
	glUniform1i(m_parameters[U_LIGHTENABLED], 0);
	glUniform1i(m_parameters[U_COLOR_TEXTURE_ENABLED], 1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, mesh->textureID);
	glUniform1i(m_parameters[U_COLOR_TEXTURE], 0);
	for (unsigned i = 0; i < text.length(); ++i)
	{
		Mtx44 characterSpacing;
		characterSpacing.SetToTranslation(i * .6f, 0, 0); //1.0f is the spacing of each character, you may change this value
		Mtx44 MVP = projectionStack.Top() * viewStack.Top() * modelStack.Top() * characterSpacing;
		glUniformMatrix4fv(m_parameters[U_MVP], 1, GL_FALSE, &MVP.a[0]);

		mesh->Render((unsigned)text[i] * 6, 6);
	}
	glBindTexture(GL_TEXTURE_2D, 0);
	glUniform1i(m_parameters[U_TEXT_ENABLED], 0);

	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();

	glEnable(GL_DEPTH_TEST);

}


void Cave::RenderMeshOnScreen(Mesh* mesh, int x, int y, int sizex, int sizey)
{
	glDisable(GL_DEPTH_TEST);
	Mtx44 ortho;
	ortho.SetToOrtho(0, 80, 0, 60, -10, 10); //size of screen UI
	projectionStack.PushMatrix();
	projectionStack.LoadMatrix(ortho);
	viewStack.PushMatrix();
	viewStack.LoadIdentity(); //No need camera for ortho mode
	modelStack.PushMatrix();
	modelStack.LoadIdentity();
	//to do: scale and translate accordingly
	RenderMesh(mesh, false); //UI should not have light
	projectionStack.PopMatrix();
	viewStack.PopMatrix();
	modelStack.PopMatrix();
	glEnable(GL_DEPTH_TEST);
}



bool Cave::Boundscheck(Vector3 position)
{
	Vector3 pos = position;
	for (int i = 0; i < NUM_GEOMETRY; ++i)
	{
		if (pos != camera.position)
		{
			if (((pos.x <= camera.position.x + 2) && (pos.x >= camera.position.x - 2)) && ((pos.y <= camera.position.y + 2) && (pos.y >= camera.position.y - 2)) && ((pos.z <= camera.position.z + 2) && (pos.z >= camera.position.z - 2)))
			{
				return true;
			}
		}
		if (objects[i] != NULL)
		{
			if (((pos.x <= objects[i]->maxX) && (pos.x >= objects[i]->minX)) && ((pos.y <= objects[i]->maxY) && (pos.y >= objects[i]->minY)) && ((pos.z <= objects[i]->maxZ) && (pos.z >= objects[i]->minZ)))
			{
				return true;
			}
		}
		if (aitypes[i] != NULL)
		{
			if (pos != aitypes[i]->position)
			{
				if (((pos.x <= aitypes[i]->maxX) && (pos.x >= aitypes[i]->minX)) && ((pos.y <= aitypes[i]->maxY) && (pos.y >= aitypes[i]->minY)) && ((pos.z <= aitypes[i]->maxZ) && (pos.z >= aitypes[i]->minZ)))
				{
					return true;
				}
			}
		}

	}
	return false;
}

void Cave::Checkrange()
{
	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (aitypes[i] != NULL)
		{
			aitypes[i]->inrange(camera.position);
		}
	}
}

void Cave::Exit()
{
	// Cleanup VBO here

	for (int i = 0; i < NUM_GEOMETRY; i++)
	{
		if (meshList[i] != NULL)
		{
			delete meshList[i];
		}
		meshList[i] = NULL;
		if (objects[i] != NULL)
		{
			delete objects[i];
		}
		objects[i] = NULL;
	}
	glDeleteProgram(m_programID);

}
